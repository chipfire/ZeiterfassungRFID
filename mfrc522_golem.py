from machine import Pin, SPI
import rp2
#import Flash

class MFRC522:
    ERR_OK		= 0
    ERR_NO_TAG	= 1
    ERR_ERR		= 2
    ERR_AUTH	= 3
    ERR_WR_DATA = 4
    
    MFRC522_CMD_CALC_CRC = 0x03
    MFRC522_CMD_TX		 = 0x04
    MFRC522_CMD_RX		 = 0x08
    # in order to read data from a tag's EEPROM we need to use the transceive command
    # as we need to transmit the command and block address. The tag then returns the respective data.
    MFRC522_CMD_TRANSCEIVE = 0x0c
    MFRC522_CMD_AUTHENT	 = 0x0e
    MFRC522_CMD_RESET	 = 0x0f
    
    currTagNUID = bytearray()
    tagPresent = False
    
    def __init__(self):
        self.cs = Pin(13, Pin.OUT)
        self.rst = Pin(0, Pin.OUT)
        
        # assert reset, it's triggered by a rising edge
        self.rst.value(0)
        # deselect MFRC522; CS is active low
        self.cs.value(1)
        
        self.spi = SPI(1, 1000000, sck=Pin(10), mosi=Pin(11), miso=Pin(12)) # use default pins: 11 -> MOSI, 8 -> MISO, 10 -> SCK
        self.spi.init()
        
        # give the chip a rising edge
        self.rst.value(1)
        self.init()
    
    def init(self):
        # perform soft reset
        self.writeReg(0x01, self.MFRC522_CMD_RESET)
        
        # set timer mode register to tell the MFRC522 to start its timer after finishing a transmission
        # (acts as a timeout). The lower 4 bits are the high order bits of the prescaler (which is set to 0xd3e)
        self.writeReg(0x2a, 0x8D)
        self.writeReg(0x2b, 0x3E)
        # write the two timer reload registers, the timer register is set to 0x0030
        self.writeReg(0x2d, 30)
        self.writeReg(0x2c, 0)
        
        # force 100% ASK modulation using TxASKReg
        self.writeReg(0x15, 0x40)
        
        val = self.readReg(0x11)
        #print(str(val) + "\n")
        
        # set ModeReg, force transmitter to only start if an RF field is generated,
        # set MFIN to be active high and select CRC preset 0x6363
        self.writeReg(0x11, 0x3D)
        
        val = self.readReg(0x11)
        #print(str(val) + "\n")
        
        self.antennaOn()
    
    def antennaOn(self):
        val = self.readReg(0x14)

        # activate the antenna by enabling both transmitter output pins using TxControlReg
        if (val & 0x3) == 0:
            self.regSetMask(0x14, 0x03)
        
        val = self.readReg(0x14)
    
    def antennaOff(self):
        # disable both transmitter output pins using TxControlReg
        self.regClearMask(0x14, 0x03)
    
    def writeReg(self, regIdx, val):
        # first write register address (6 bits), bit 7 must be 0 to indicate write, bit 0 must be 0
        txData = bytearray(b'%c%c' % (int(((regIdx & 0x3f) << 1)), int(val & 0xff)))
        
        # assert chip select
        self.cs.value(0)
        
        # write value
        self.spi.write(txData)
        
        # de-assert chip select
        self.cs.value(1)
    
    def readReg(self, regIdx):
        # create request
        txData = bytearray(b'%c\x00' % int(((regIdx & 0x3f) << 1) | 0x80))
        rxData = bytearray(2)
        
        # almost the same as above
        self.cs.value(0)
        
        # this is a bit shorter, it allows us to send and receive simultaneously.
        self.spi.write_readinto(txData, rxData)
        
        self.cs.value(1)
        
        #print(rxData)
        
        # return the value we read, result is of type bytes (array).
        # Byte 0 contains meaningless data, it's what was received when we sent the request.
        return rxData[1]
    
    # two helper functions for setting and clearing bits in 
    def regSetMask(self, regIdx, mask):
        self.writeReg(regIdx, self.readReg(regIdx) | mask)
    
    def regClearMask(self, regIdx, mask):
        self.writeReg(regIdx, self.readReg(regIdx) & (~mask))
    
    def toChip(self, cmd, txData):
        rxData = bytearray()
        # for some commands we want to wait until they finish. We can tell this from
        # the different interrupts.
        bits = irqEn = waitIrq = irqReg = 0
        stat = self.ERR_ERR
        
        # commands only use the four LSBits
        cmd = cmd & 0x0f
                
        # determine which interrupts to enable and to consider when waiting for the command
        # to finish. Idle and error interrupt are monitored for all commands, for transceive
        # operations we also monitor the rxEnd interrupt. It tells us that the chip is no longer
        # receiving any data.
        if cmd == self.MFRC522_CMD_AUTHENT:
            irqEn = 0x12
            waitIrq = 0x12
        elif cmd == self.MFRC522_CMD_TRANSCEIVE:
            irqEn = 0x77
            waitIrq = 0x32
        
        # enable this line to have enabled interrupts routed to the IRQ pin
        self.writeReg(0x02, irqEn | 0x80)
        # clear all interrupt bits
        self.regClearMask(0x04, 0x80)
        # flush FIFO by writing bit 7 of FIFOLevelReg (clears read and write pointer to 0)
        self.regSetMask(0x0A, 0x80)
        self.writeReg(0x01, 0x00)
        
        # put all data to transmit into the MFRC522's FIFO
        for c in txData:
            self.writeReg(0x09, c)
        # now write the command into CommandReg, telling the chip what to do
        self.writeReg(0x01, cmd)
        
        # start transmission; however, this only needs to be done for transceive command
        if cmd == self.MFRC522_CMD_TRANSCEIVE:
            self.regSetMask(0x0D, 0x80)
        
        # wait until the command is finished or the MFRC522's timer expires
        irqReg = 0
        while ((irqReg & waitIrq) == 0) and (((irqReg & irqEn) & 0x01) == 0):
            irqReg = self.readReg(0x04)
                
        # reset StartSend flag
        self.regClearMask(0x0D, 0x80)
        
        # check whether the timer expired
        if ((irqReg & irqEn) & 0x01) != 0:
            # there was no response, signal no tag error (the timer is activated automatically)
            stat = self.ERR_NO_TAG
        else:
            # check the error register, only proceed if the chip doesn't indicate an error
            if (self.readReg(0x06) & 0x1B) == 0x00:
                # success!
                stat = self.ERR_OK
                
                # in case a transceive command was issued, read the returned data from the FIFO
                if cmd == 0x0C:
                    # read FIFO level register to determine how many bytes we got
                    nBytes = self.readReg(0x0A)
                    # some of the last byte's bits may be invalid, the ControlReg tells us how many
                    # are valid (0 means all).
                    lastBits = self.readReg(0x0C) & 0x07
                    if lastBits != 0:
                        bits = (nBytes - 1) * 8 + lastBits
                    else:
                        bits = nBytes * 8
                    
                    # now read the bytes returned by the tag into the receive buffer by reading
                    # from the FIFO register
                    for _ in range(nBytes):
                        rxData.append(self.readReg(0x09))

        return stat, rxData, bits
    
    def addCRC(self, data):
        # clear the CRC unit's interrupt
        self.regClearMask(0x05, 0x04)
        # clear FIFO
        self.regSetMask(0x0A, 0x80)
        
        # put data into the FIFO, the CRC unit will fetch it from there and do its calculations
        for c in data:
            self.writeReg(0x09, c)
        
        # write the command to start CRC calculation to the command register
        self.writeReg(0x01, self.MFRC522_CMD_CALC_CRC)
        
        irqReg = 0
        # wait until the CRC unit has processed all data by polling DivIrqReg
        while irqReg != 0x04:
            irqReg = self.readReg(0x05)
        
        # write idle command to command register in order to stop CRC calculation
        self.writeReg(0x01, 0x00)
        
        return data + bytearray(b'%c%c' % (self.readReg(0x22), self.readReg(0x21)))
    
    def requestTags(self):
        status = None
        resultData = None
        # only request Type A tags, Type B tags require a different configuration
        # of the MFRC522 and a different activation command.
        tagType = bytes(b'\x26')
        
        # configure BitFramingReg for transmission of a command to wake idle tags
        # REQA is a 7 Bit command, so we must configure 
        self.writeReg(0x0d, 0x07)
        
        # add request mode and start the request
        (status,resultData,returnedBits) = self.toChip(self.MFRC522_CMD_TRANSCEIVE, tagType)
        
        # check whether the result is what we expect:
        if (status != self.ERR_OK) or (returnedBits != 0x10):
            status = self.ERR_ERR
        
        return (status,returnedBits)
    
    def anticoll(self):
        resultData = bytearray()
        localBCC = 0
        
        # put the command to initiate the anti collision sequence into a byte array for transmission
        # 0x20 is the NVB which contains the number of valid bytes in the upper nibble (at least 2
        # as here)
        tagCmd = bytearray(b'\x93\x20')
                
        # clear bit framing register
        self.writeReg(0x0d, 0x00)
        
        (status,resultData,backBits) = self.toChip(self.MFRC522_CMD_TRANSCEIVE,tagCmd)
        
        # byte 5 is the Block Check Character; it's UID0 ^ UID1 ^ UID2 ^ UID3
        # we need to check whether the UID we received matches the BCC to verify data integrity
        if(status == self.ERR_OK):
            print("okay, data: " + str(resultData) + "\n")
        
            i = 0
            if len(resultData)==5:
                for i in range(4):
                    localBCC = localBCC ^ resultData[i]
                
                if localBCC != resultData[4]:
                    print("BCC check failed: " + str(localBCC) + " (calculated) != " + str(resultData[4]) + " (received)\n")
                    status = self.ERR_ERR
            else:
                status = self.ERR_ERR
                
        return (status,resultData)

    def selectTag(self, serNum):
        resultData = bytearray()
        
        # put the "select tag" command into a transmit buffer and append five bytes (UID + BCC) of
        # the serial number...
        txBuffer = self.addCRC(bytearray(b'\x93\x70') + serNum[:5])
        
        print("MFRC522::SelectTag(): sending this to the tag: " + str(txBuffer) + "\n")
        
        (status, rxBuffer, rxBits) = self.toChip(self.MFRC522_CMD_TRANSCEIVE, txBuffer)
        
        if (status == self.ERR_OK) and (rxBits == 0x18):
            print("SAK: " + str(rxBuffer[0]) + "\n")
            return rxBuffer[0]
        else:
            return 0
    
    def activateTag(self):
        # perform anti collision sequence and get the winning tag's NUID
        (status,uid) = self.anticoll()
        
        # activate tag if anti collision sequence was successful
        if status != self.ERR_OK:
            return self.ERR_ERR
        else:
            # Print NUID
            print("tag\'s NUID: " + str(uid) + " lastNUID: " + str(self.currTagNUID) + "\n")
            
            # Select the scanned tag
            result = self.selectTag(uid)
            
            if result == 0:
                return self.ERR_ERR
            
            # update NUID variable
            self.currTagNUID = uid
            
            self.tagPresent = True
        
        return self.ERR_OK
    
    def tagAuthenticate(self, authMode, blockAddr, blockKey, nuid):
        # in order to authenticate we need to transmit the proper command, block address,
        # sector key and the tag's NUID - in that order. Note that we're not sending the BCC!
        txBuffer = bytearray(b'%c%c' % (authMode, blockAddr)) + blockKey + nuid[:4]

        print("MFRC522::tagAuthenticate(): sending this to tag: " + str(txBuffer))

        # Now we start the authentication itself
        (status, rxBuffer, backLen) = self.toChip(self.MFRC522_CMD_AUTHENT,txBuffer)
        
        # Check if an error occurred. If so, Status2Reg could be checked to see whether the
        # crypto engine is on. We're not doing this here as it doesn't give us any more insight.
        if status != self.ERR_OK:
            return self.ERR_AUTH
        
        # Return the status
        return status
    
    def readTagBlock(self, blockNumber, blockKey = bytes(b'\xff\xff\xff\xff\xff\xff')):
        if (blockNumber < 0) or (blockNumber > 63):# or not self.tagPresent:
            return (self.ERR_ERR, bytes())
        
        rxBuffer = bytes()
        status = self.ERR_ERR
        
        # use key A to authenticate; in order to use key B, tag command 0x61 (Authent1B)
        # has to be used.
        status = self.tagAuthenticate(0x60, blockNumber, blockKey, self.currTagNUID)
        
        # Check if authentication was successful
        if status != self.ERR_OK:
            return (self.ERR_AUTH, bytes())
        else:
            txBuffer = self.addCRC(bytearray(b'\x30%c' % blockNumber))
            
            print("MFRC522::readTagBlock(): sending this to tag: " + str(txBuffer))
            
            (status, rxBuffer, rxBits) = self.toChip(self.MFRC522_CMD_TRANSCEIVE, txBuffer)
        
        return (status, rxBuffer)
    
    def writeTagBlock(self, blockNumber, data, blockKey = bytes(b'\xff\xff\xff\xff\xff\xff')):
        # block 0 is always read-only
        if blockNumber < 1 or blockNumber > 63 or not self.tagPresent:
            return self.ERR_ERR
        
        # same as in the read command above
        status = self.tagAuthenticate(0x60, blockNumber, blockKey, self.currTagNUID)
        
        # Check if authentication was successful
        if status != self.ERR_OK:
            return self.ERR_AUTH
        else:
            # first we need to send the tag a write command, the data to be written is only
            # transmitted after the tag has ackowledged the command and switched to write mode.
            txBuffer = self.addCRC(bytes(b'\xa0%c' % blockNumber))
            (status, rxBuffer, rxBits) = self.toChip(self.MFRC522_CMD_TRANSCEIVE, txBuffer)
            
            # check the result; if everything went fine the tag will return a four bit
            # ACK, encoded as 0xa
            if (status != self.ERR_OK) or (rxBits != 4) or ((rxBuffer[0] & 0x0f) != 0x0a):
                return self.ERR_ERR
            else:
                # prepare the data buffer; it should always contain 16 data bytes.
                if len(data) < 16:
                    txBuffer = self.addCRC(data + bytes(16 - len(data)))
                else:
                    txBuffer = self.addCRC(data[:16])
                
                (status, rxBuffer, rxBits) = self.toChip(self.MFRC522_CMD_TRANSCEIVE, txBuffer)
                
                # the tag will again send an ACK if data was written succesfully
                if (status != self.ERR_OK) or (rxBits != 4) or ((rxBuffer[0] & 0x0f) != 0x0a):
                    status = self.ERR_WR_DATA
        
        return status


reader = MFRC522()
tagUid = bytearray()

while True:
    # scan for idle tags
    (status, returnedBits) = reader.requestTags()
    
    if status == MFRC522.ERR_OK:
        break;

#(status, uid) = reader.anticoll()
#tagUid = uid
#
#print(str(tagUid) + "\n")

if reader.activateTag() != MFRC522.ERR_OK:
        print("Activating tag failed!\n")
else:
    status = reader.writeTagBlock(1, bytes(b'Hallo, liebe'))
    
    if status == MFRC522.ERR_OK:
        print("Succesfully wrote block 1")
    else:
        print("Writing block 1 failed: " + str(status))
    
    status = reader.writeTagBlock(2, bytes(b'Golem-Leser!'))
    
    if status == MFRC522.ERR_OK:
        print("Succesfully wrote block 2")
    else:
        print("Writing block 2 failed: " + str(status))
    
    for block in range(4):
        (status, rxData) = reader.readTagBlock(block)

        if status == MFRC522.ERR_OK:
            print("Succesfully read block " + str(block) + ", data: " + str(rxData))
        else:
            print("Reading block " + str(block) + " failed: " + str(status))