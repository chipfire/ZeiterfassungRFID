# ZeiterfassungRFID

Eine Micropython-Anwendung, um mit dem Raspberry Pi Pico W ein Zeiterfassungssystem aufzubauen.
Zur Erfassung werden RFID-Tags verwendet, die über eine RFID-RC522-Platine mit MFRC522-Chip ausgelesen werden. Zur Verwaltung gibt es eine Web-Oberfläche. Darüber können Mitarbeiter und Tags hinzugefügt und gelöscht werden, die Daten sollen sich darüber künfgit als CSV-Datei exportieren lassen (fehlt aktuell noch).
Gespeichert werden die erfassten Zeitdaten und Informationen zu den Tags im Flash-Speicher des Boards. Dazu muss Micropython selbst kompiliert werden, um die Partition, auf der Python-Dateien im Flash gespeichert werden, zu verkleinern. Andernfalls kommt es zu Konflikten mit den von der Anwendung gespeicherten Daten.
Aktuell ist die Entwicklung noch nicht abgeschlossen.